package io.tesla.proviso.archive;

public class ZipArchiverTypeTest extends ArchiveTypeTest {
  @Override
  protected String getArchiveExtension() {
    return "zip";
  }
}
