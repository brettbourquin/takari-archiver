package io.tesla.proviso.archive;

public class TarGzArchiverTypeTest extends ArchiveTypeTest {
  @Override
  protected String getArchiveExtension() {
    return "tar.gz";
  }
}
